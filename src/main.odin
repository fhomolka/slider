package Slider

import rl "vendor:raylib"
import "core:fmt"
import "core:os"
import "core:strings"
import "core:strconv"
import "core:encoding/xml"

main :: proc()
{
	if len(os.args) < 2
	{
		fmt.println("No presentation loaded!");
		return;
	}

	presentation: Presentation;

	doc: ^xml.Document;
	err: xml.Error;
	doc, err = xml.load_from_file(os.args[1]);
	if err != .None
	{
		fmt.println("Could not load file ", os.args[1]);
		return
	}

	for i:= 0; i < len(doc.elements); i += 1
	{
		elem: xml.Element = doc.elements[i];
		attribs: xml.Attributes = elem.attribs;

		if elem.ident == "presentation" 
		{
			for j:= 0; j < len(attribs); j += 1
			{
				attrib: xml.Attribute = attribs[j];
				if attrib.key == "title"
				{
					presentation.title = attrib.val;
					break;
				}
			}

			for j:= 0; j < len(elem.children); j += 1
			{
				child := doc.elements[elem.children[j]];
				if child.ident == "slide" 
				{
					new_slide : Slide;
					new_slide.xml_doc_id = elem.children[j];
					append(&presentation.slides, new_slide)	
				}
			}
			
			break;
		}
	}

	rl.SetTraceLogLevel(rl.TraceLogLevel.WARNING)
	window_title: cstring = strings.clone_to_cstring(presentation.title);

	rl.InitWindow(1440, 900, window_title);
	defer rl.CloseWindow();
	rl.SetTargetFPS(60);

	for i:= 0; i < len(presentation.slides); i += 1
	{
		xml_slide := doc.elements[presentation.slides[i].xml_doc_id];

		for j:= 0; j < len(xml_slide.children); j += 1
		{
			child := doc.elements[xml_slide.children[j]];
			attribs: xml.Attributes = doc.elements[xml_slide.children[j]].attribs;

			new_element : Element;
			new_element.xml_doc_id = xml_slide.children[j];
			new_element.scale = 1;

			switch child.ident 
			{
				case "text_box":
					new_element.text =  doc.elements[new_element.xml_doc_id].value;
					new_element.color = rl.BLACK
					new_element.draw = TextBox_draw;
					new_element.scale = 20
				case "image":
					new_element.color = rl.WHITE
					new_element.draw = Image_draw;
				case:
					fmt.printf("%s type not recognized or implemented!\n", child.ident)
			}

			for j:= 0; j < len(attribs); j += 1
			{
				attrib: xml.Attribute = attribs[j];

				switch attrib.key 
				{
					case "pos_x":
						if strings.has_suffix(attrib.val, "%") 
						{
							perc := strings.trim_right(attrib.val, "%")
							newval : f32 = f32(strconv.atof(perc)) / 100
							newval *= f32(rl.GetScreenWidth())
							new_element.pos.x = newval;
						}
						else
						{
							new_element.pos.x = f32(strconv.atof(attrib.val));
						}
					case "pos_y":
						if strings.has_suffix(attrib.val, "%") 
						{
							perc := strings.trim_right(attrib.val, "%")
							newval : f32 = f32(strconv.atof(perc)) / 100
							newval *= f32(rl.GetScreenHeight())
							new_element.pos.y = newval;
						}
						else
						{
							new_element.pos.y = f32(strconv.atof(attrib.val));
						}
					case "scale":
						if strings.has_suffix(attrib.val, "%")
						{
							perc := strings.trim_right(attrib.val, "%")
							newval : f32 = f32(strconv.atof(perc)) / 100
							new_element.scale = newval;
						}
						else
						{
							new_element.scale = f32(strconv.atof(attrib.val));
						}
					case "color":
						switch attrib.val 
						{
							case "white":
								new_element.color = rl.WHITE
							case "red":
								new_element.color = rl.RED
							case "green":
								new_element.color = rl.GREEN
							case "blue":
								new_element.color = rl.BLUE
							case "black":
								new_element.color = rl.BLACK
							case:
								r_str := attrib.val[0:2];
								g_str := attrib.val[2:4];
								b_str := attrib.val[4:6];
								r, g, b:u8;
								ok: bool;
								temp: uint;

								temp, ok = strconv.parse_uint(r_str, 16)
								new_element.color.r = u8(temp)
								temp, ok = strconv.parse_uint(g_str, 16)
								new_element.color.g = u8(temp)
								temp, ok = strconv.parse_uint(b_str, 16)
								new_element.color.b = u8(temp)
						}
					case "src":
						cstr: cstring = strings.clone_to_cstring(attrib.val)
						new_element.texture = rl.LoadTexture(cstr)
					case:
						fmt.printf("Attribute %s not recognized or implemented", attrib.key)
					
				}

			}

			append(&presentation.slides[i].elements, new_element)	
		}
	}

	//xml.destroy(doc)

	current_slide_id :u32 = 0;

	for !rl.WindowShouldClose()
	{
		if rl.IsKeyPressed(rl.KeyboardKey.RIGHT) 
		{
			if current_slide_id < u32(len(presentation.slides) - 1) 
			{
				
				current_slide_id += 1
			}	
		}
		if rl.IsKeyPressed(rl.KeyboardKey.LEFT) 
		{
			if current_slide_id > 0 
			{
				current_slide_id -= 1
			}	
		}

		current_slide := presentation.slides[current_slide_id];

		rl.BeginDrawing();
		defer rl.EndDrawing();

		rl.ClearBackground(rl.RAYWHITE);

		for i := 0; i < len(current_slide.elements); i += 1
		{
			//TODO(fhomolka): Replace with switch-case
			current_slide.elements[i].draw(&current_slide.elements[i]);
		}
	}
}