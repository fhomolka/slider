package Slider

import rl "vendor:raylib"
import "core:encoding/xml"
import "core:strings"

// Slide elements
Element :: struct
{
	xml_doc_id: u32,
	pos: rl.Vector2,
	scale: f32,
	text: string,
	color: rl.Color,
	texture: rl.Texture,

	process: proc(),
	draw: proc(tbox: ^Element),
}

// Presentation elements
Slide :: struct
{
	xml_doc_id: u32,
	elements: [dynamic]Element
}

Presentation :: struct
{
	xml_elem_id: xml.Element_ID,
	title: string,
	slides: [dynamic]Slide,
}


// element procedures

TextBox_draw :: proc(tbox: ^Element)
{
	cstr : cstring = strings.clone_to_cstring(tbox.text);
	rl.DrawText(cstr, i32(tbox.pos.x), i32(tbox.pos.y), i32(tbox.scale), tbox.color);
}

Image_draw :: proc(img: ^Element)
{
	rl.DrawTextureEx(img.texture, img.pos, 0, img.scale, img.color);
}